package com.spotybot.config;

import com.spotybot.model.MySpotyTelegramBot;
import com.spotybot.botapi.BotFacade;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Класс конфигурации бина бота.
 * Конфигурационный класс создан, чтобы создавать бины с уже прописанными типовыми свойствами
 */
@Getter
@Setter
@Configuration
public class BotConfig {
    /**
     * локальная url бота
     */
    @Value("${telegram-bot.webhook-path}")
    private String webhookPath;
    /**
     * имя бота
     */
    @Value("${telegram-bot.username}")
    private String botUsername;
    /**
     * токен бота
     */
    @Value("${telegram-bot.bot-token}")
    private String botToken;

    /**
     * @param facade - отдельный фасад, в который мы вынесли обработку приходящего Update'а
     */
    @Bean
    @Autowired
    public MySpotyTelegramBot bot(BotFacade facade) {
        var bot = new MySpotyTelegramBot(webhookPath,botUsername,botToken);
        bot.setFacade(facade);
        return bot;
    }
}