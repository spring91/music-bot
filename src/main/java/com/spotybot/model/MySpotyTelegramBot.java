package com.spotybot.model;

import com.spotybot.botapi.BotFacade;
import lombok.Setter;
import org.telegram.telegrambots.bots.TelegramWebhookBot;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;

/**
 * Класс бота
 */
@Setter
public class MySpotyTelegramBot extends TelegramWebhookBot {

    private String webHookPath;
    private String botUsername;
    private String botToken;
    private BotFacade facade;

    public MySpotyTelegramBot(String webHookPath,
                              String botUsername,
                              String botToken) {
        this.webHookPath = webHookPath;
        this.botUsername = botUsername;
        this.botToken = botToken;
    }

    @Override
    public String getBotUsername() {
        return botUsername;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    @Override
    public String getBotPath() {
        return webHookPath;
    }

    /**
     * Метод принятия и обработки сообщений с ответом (на нем строится вся работа бота)
     *
     * @param update - принимает распарсенный JSON в виде объекта Update
     * @return BotApiMethod -  и возвращает, то, что хочет от нас «услышать» сервер telegram.
     */
    @Override
    public BotApiMethod<?> onWebhookUpdateReceived(Update update) {
        return facade.handleUpdate(update);
    }
}