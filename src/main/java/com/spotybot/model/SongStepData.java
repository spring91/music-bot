package com.spotybot.model;

import lombok.Data;

import javax.annotation.Nullable;

/**
 * Данные анкеты пользователя
 */
@Data
public class SongStepData {
    @Nullable private String artist;
    @Nullable private String song;
    @Nullable private String album;
}