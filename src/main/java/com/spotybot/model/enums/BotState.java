package com.spotybot.model.enums;

import com.spotybot.botapi.msghandl.impl.AlbumLinkHandlerImpl;
import com.spotybot.botapi.msghandl.impl.BaseFunctionHandlerImpl;
import com.spotybot.botapi.msghandl.impl.MusicQuestionerHandlerImpl;
import lombok.Getter;

/**
 * Возможные состояния бота
 */
@Getter
public enum BotState {

    SHOW_MAIN_MENU(BaseFunctionHandlerImpl.class),
    SHOW_HELP_TEXT(BaseFunctionHandlerImpl.class),

    ASK_ARTIST(MusicQuestionerHandlerImpl.class),
    ASK_SONG(MusicQuestionerHandlerImpl.class),
    SONG_STEP_FILLED(MusicQuestionerHandlerImpl.class),

    ASK_ALBUM_ARTIST(AlbumLinkHandlerImpl.class),
    ASK_MUSIC_ALBUM(AlbumLinkHandlerImpl.class),
    ALBUM_STEP_FILLED(AlbumLinkHandlerImpl.class);

    /**
     * Поле указывает на обработчика данного состояния
     */
    private final Class<?> handler;

    BotState(Class<?> handler) {
        this.handler = handler;
    }
}