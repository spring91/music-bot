package com.spotybot.model;

import com.spotybot.model.enums.BotState;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Person {

    /**
     * Id юзера в телеграмме
     */
    private int chatId;
    /**
     * Поле состояния бота в многоуровневом ответе
     */
    private BotState botState;
}