package com.spotybot.rest.handler;

import com.spotybot.rest.handler.exc.ResultNullSuccessException;
import com.spotybot.rest.handler.exc.SessionStateException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

/**
 *  Контроллер ловит ошибки и отправляет пользователю сообщения о них
 */
@RestControllerAdvice
public class ExceptionHandlerAdvice {

    @ExceptionHandler(value = SessionStateException.class)
    public BotApiMethod<?> exception(SessionStateException exception) {
        return new SendMessage(String.valueOf(exception.getChatID()), exception.getMessage());
    }

    @ExceptionHandler(value = ResultNullSuccessException.class)
    public BotApiMethod<?> resultNullSuccessException(ResultNullSuccessException exception) {
        return new SendMessage(String.valueOf(exception.getChatID()), exception.getMessage());
    }
}