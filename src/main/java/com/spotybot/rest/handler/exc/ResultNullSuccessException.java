package com.spotybot.rest.handler.exc;

import lombok.Getter;

/**
 * Ошибка поиска трека на spotify
 */
@Getter
public class ResultNullSuccessException extends RuntimeException {

    private final long chatID;

    public ResultNullSuccessException(String message, long chatID) {
        super(message);
        this.chatID = chatID;
    }
}