package com.spotybot.rest.handler.exc;

import lombok.Getter;

/**
 * Ошибка состояния бота
 */
@Getter
public class SessionStateException extends RuntimeException {

    private final long chatID;

    public SessionStateException(String message, long chatID) {
        super(message);
        this.chatID = chatID;
    }
}
