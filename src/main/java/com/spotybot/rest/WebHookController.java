package com.spotybot.rest;

import com.spotybot.model.MySpotyTelegramBot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;

@RestController
@RequestMapping("/")
public class WebHookController {
    private final MySpotyTelegramBot telegramBot;

    @Autowired
    public WebHookController(MySpotyTelegramBot telegramBot) {
        this.telegramBot = telegramBot;
    }

    @PostMapping()
    public BotApiMethod<?> onUpdateReceived(@RequestBody Update update) {
        return telegramBot.onWebhookUpdateReceived(update);
    }
}