package com.spotybot.service;

public interface SellerSpotifyService {

    /**
     * Получение ссылки на трек или альбом из Spotify
     *
     * @param searchParameter параметры запроса
     */
    String getLink(String searchParameter);
}