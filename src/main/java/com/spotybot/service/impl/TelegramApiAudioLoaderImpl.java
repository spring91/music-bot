package com.spotybot.service.impl;

import com.spotybot.service.TelegramApiDataLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.text.MessageFormat;

/**
 * Класс отправки сложных данных(отправка файлов песен)
 */
@Service
public class TelegramApiAudioLoaderImpl implements TelegramApiDataLoader {
    private final String url;
    private final String botToken;
    private final RestTemplate restTemplate;

    @Autowired
    public TelegramApiAudioLoaderImpl(@Value("${telegram-bot.url}") String url,
                                      @Value("${telegram-bot.bot-token}") String botToken,
                                      RestTemplate restTemplate) {
        this.url = url;
        this.botToken = botToken;
        this.restTemplate = restTemplate;
    }

    public void uploadFile(long chatId, String value) {
        var map = new LinkedMultiValueMap<String, Object>();
        map.add("audio", value);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        var requestEntity = new HttpEntity<>(map, headers);


        ResponseEntity<String> jsonNodeResponseEntity = restTemplate.exchange(
                MessageFormat.format("{0}bot{1}/sendAudio?chat_id={2}", url, botToken, String.valueOf(chatId)),
                HttpMethod.POST,
                requestEntity,
                String.class);
        assert jsonNodeResponseEntity.getBody() != null : "RestError";
    }
}