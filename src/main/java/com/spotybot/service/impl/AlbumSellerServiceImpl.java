package com.spotybot.service.impl;

import com.spotybot.service.SellerSpotifyService;
import lombok.extern.slf4j.Slf4j;
import org.apache.hc.core5.http.ParseException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import se.michaelthelin.spotify.SpotifyApi;
import se.michaelthelin.spotify.exceptions.SpotifyWebApiException;
import se.michaelthelin.spotify.model_objects.specification.AlbumSimplified;
import se.michaelthelin.spotify.model_objects.specification.Paging;

import java.io.IOException;

/**
 * Класс поиска ссылки на альбом spotify
 */
@Slf4j
@Service
public class AlbumSellerServiceImpl implements SellerSpotifyService {

    private final SpotifyApi spotifyApi;

    @Autowired
    public AlbumSellerServiceImpl(SpotifyApi spotifyApi) {
        this.spotifyApi = spotifyApi;
    }

    @Override
    public String getLink(String searchParameter) {
        Paging<AlbumSimplified> albumPaging;
        try {
            albumPaging = spotifyApi.searchAlbums(searchParameter)
                    .build().execute();
            if (albumPaging.getTotal() > 0)
                return albumPaging.getItems()[0].getExternalUrls().toString();
        } catch (IOException | ParseException | NullPointerException | SpotifyWebApiException e) {
            log.warn(e.getMessage());
        }
        return null;
    }
}