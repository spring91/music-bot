package com.spotybot.service.impl;

import com.spotybot.service.SellerSpotifyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import se.michaelthelin.spotify.SpotifyApi;
import se.michaelthelin.spotify.exceptions.SpotifyWebApiException;
import se.michaelthelin.spotify.model_objects.specification.Paging;
import se.michaelthelin.spotify.model_objects.specification.Track;

import java.io.IOException;

/**
 * Класс поиска ссылки трека spotify
 */
@Slf4j
@Service
public class TrackSellerServiceImpl implements SellerSpotifyService {
    private final SpotifyApi spotifyApi;

    @Autowired
    public TrackSellerServiceImpl(SpotifyApi spotifyApi) {
        this.spotifyApi = spotifyApi;
    }

    @Override
    public String getLink(String searchParameter) {
        Paging<Track> trackPaging;
        try {
            trackPaging = spotifyApi.searchTracks(searchParameter)
                    .build().execute();
            if (trackPaging.getTotal() > 0)
                return trackPaging.getItems()[0].getPreviewUrl();
        } catch (IOException | SpotifyWebApiException | org.apache.hc.core5.http.ParseException e) {
            //регистрационный токен Spotify может оказаться недействительным
            log.warn(e.getMessage());
        }
        return null;
    }
}