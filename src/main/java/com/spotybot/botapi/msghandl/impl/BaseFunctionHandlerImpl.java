package com.spotybot.botapi.msghandl.impl;

import com.spotybot.botapi.msghandl.InputMessageHandler;
import com.spotybot.dao.PersonDAO;
import com.spotybot.model.enums.BotState;
import com.spotybot.rest.handler.exc.SessionStateException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

/**
 * Класс занимается обработкой базовых запросов
 */
@Component
public class BaseFunctionHandlerImpl implements InputMessageHandler {

    private final PersonDAO personDAO;
    private final String mainMenu;
    private final String help;

    @Autowired
    public BaseFunctionHandlerImpl(@Value("${messages.base-bot-state.menu}") String mainMenu,
                                   @Value("${messages.base-bot-state.help}") String help,
                                   PersonDAO personDAO) {
        this.personDAO = personDAO;
        this.mainMenu = mainMenu;
        this.help = help;
    }

    @SneakyThrows
    @Override
    public BotApiMethod<?> handle(@NotNull Message inputMsg) {
        long chatId = inputMsg.getChatId();
        int userId = inputMsg.getFrom().getId().intValue();
        var person = personDAO.getPerson(userId);
        if (person == null)
            throw new SessionStateException("Пользователь не прошел начальную регистрацию", inputMsg.getChatId());

        var botState = person.getBotState();
        //TODO сделать отправку всех кнопок в арсенале
        if (botState == BotState.SHOW_HELP_TEXT)
            return new SendMessage(String.valueOf(chatId), help);
        return new SendMessage(String.valueOf(chatId), mainMenu);
    }
}