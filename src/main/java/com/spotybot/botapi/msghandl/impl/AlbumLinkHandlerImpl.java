package com.spotybot.botapi.msghandl.impl;

import com.spotybot.dao.PersonDAO;
import com.spotybot.dao.SongStepDAO;
import com.spotybot.rest.handler.exc.ResultNullSuccessException;
import com.spotybot.rest.handler.exc.SessionStateException;
import com.spotybot.service.SellerSpotifyService;
import com.spotybot.botapi.msghandl.InputMessageHandler;
import com.spotybot.service.impl.AlbumSellerServiceImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

import static com.spotybot.model.enums.BotState.*;

/**
 * Класс занимается обработкой запросов по поиску альбома
 */
@Component
public class AlbumLinkHandlerImpl implements InputMessageHandler {

    private final PersonDAO personDAO;
    private final SongStepDAO userDataCache;
    private final SellerSpotifyService sellerSpotifyService;

    @Autowired
    public AlbumLinkHandlerImpl(PersonDAO personDAO,
                                SongStepDAO userDataCache,
                                AlbumSellerServiceImpl sellerSpotifyService) {
        this.personDAO = personDAO;
        this.userDataCache = userDataCache;
        this.sellerSpotifyService = sellerSpotifyService;
    }

    /**
     * @return сгенерированное сообщение
     */
    @Override
    public BotApiMethod<?> handle(@NotNull Message inputMsg) {
        var usersAnswer = inputMsg.getText();
        int userId = inputMsg.getFrom().getId().intValue();
        long chatId = inputMsg.getChatId();

        var person = personDAO.getPerson(userId);
        var data = userDataCache.getData(userId);
        if (person == null || data == null)
            throw new SessionStateException("Пользователь не прошел начальную регистрацию", inputMsg.getChatId());

        SendMessage replyToUser;
        var botState = person.getBotState();
        switch (botState) {
            case ASK_MUSIC_ALBUM: {
                data.setArtist(usersAnswer);
                person.setBotState(ALBUM_STEP_FILLED);
                replyToUser = new SendMessage(String.valueOf(chatId), "Введите название альбома");
                break;
            }
            case ALBUM_STEP_FILLED: {
                data.setAlbum(usersAnswer);
                person.setBotState(SHOW_MAIN_MENU);
                var link = sellerSpotifyService.getLink(data.getArtist() + " " + data.getAlbum());
                if (link == null)
                    throw new ResultNullSuccessException("Поиск неудачен", chatId);
                replyToUser = new SendMessage(String.valueOf(chatId), link);
                break;
            }
            default: { //ASK_ALBUM_ARTIST
                person.setBotState(ASK_MUSIC_ALBUM);
                replyToUser = new SendMessage(String.valueOf(chatId), "Введите псевдоним артиста");
            }
        }
        personDAO.savePerson(person);
        userDataCache.saveData(userId, data);
        return replyToUser;
    }
}