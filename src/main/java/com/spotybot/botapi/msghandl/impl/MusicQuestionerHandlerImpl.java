package com.spotybot.botapi.msghandl.impl;

import com.spotybot.dao.PersonDAO;
import com.spotybot.dao.SongStepDAO;
import com.spotybot.rest.handler.exc.ResultNullSuccessException;
import com.spotybot.rest.handler.exc.SessionStateException;
import com.spotybot.service.SellerSpotifyService;
import com.spotybot.service.TelegramApiDataLoader;
import com.spotybot.botapi.msghandl.InputMessageHandler;
import com.spotybot.service.impl.TelegramApiAudioLoaderImpl;
import com.spotybot.service.impl.TrackSellerServiceImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

import static com.spotybot.model.enums.BotState.*;

/**
 * Класс занимается обработкой запросов на скачивание песни
 */
@Component
public class MusicQuestionerHandlerImpl implements InputMessageHandler {
    private final PersonDAO personDAO;
    private final SongStepDAO userDataCache;
    private final SellerSpotifyService spotifyData;
    private final TelegramApiDataLoader dataLoader;

    @Autowired
    public MusicQuestionerHandlerImpl(PersonDAO personDAO,
                                      SongStepDAO songStepDAO,
                                      TrackSellerServiceImpl spotifyData,
                                      TelegramApiAudioLoaderImpl dataLoader) {
        this.personDAO = personDAO;
        this.userDataCache = songStepDAO;
        this.spotifyData = spotifyData;
        this.dataLoader = dataLoader;
    }

    /**
     * @return сгенерированное сообщение
     */
    @Override
    public BotApiMethod<?> handle(@NotNull Message inputMsg) {
        var usersAnswer = inputMsg.getText();
        int userId = inputMsg.getFrom().getId().intValue();
        long chatId = inputMsg.getChatId();

        var person = personDAO.getPerson(userId);
        var songStepData = userDataCache.getData(userId);
        if (person == null || songStepData == null)
            throw new SessionStateException("Пользователь не прошел начальную регистрацию", inputMsg.getChatId());

        //TODO: что если в фасаде изначально лежат неверные токены, и юзер перескочил какую-либо инстанцию
        SendMessage replyToUser;
        var botState = person.getBotState();
        switch (botState) {
            case ASK_SONG: {
                songStepData.setArtist(usersAnswer);
                person.setBotState(SONG_STEP_FILLED);
                replyToUser = new SendMessage(
                        String.valueOf(chatId),
                        "Введите название трека для скачивания"
                );
                break;
            }
            case SONG_STEP_FILLED: {
                songStepData.setSong(usersAnswer);
                person.setBotState(SHOW_MAIN_MENU);

                var song = spotifyData.getLink(
                        songStepData.getArtist() +
                                " " + songStepData.getSong()
                );
                if (song == null)
                    throw new ResultNullSuccessException("Поиск неудачен", chatId);
                //простое SendAudio не работает
                dataLoader.uploadFile(chatId, spotifyData.getLink(
                        songStepData.getArtist() + " " + songStepData.getSong()));
                return null;
            }
            default: { //ASK_ARTIST
                person.setBotState(ASK_SONG);
                replyToUser = new SendMessage(String.valueOf(chatId), "Введите псевдоним артиста");
                break;
            }
        }
        personDAO.savePerson(person);
        userDataCache.saveData(userId, songStepData);
        return replyToUser;
    }
}