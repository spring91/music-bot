package com.spotybot.botapi.msghandl;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Message;

/**
 * Интерфейс обработчиков сообщений, занимаются поиском сообщений по состоянию бота
 */
public interface InputMessageHandler {
    BotApiMethod<?> handle(Message message);
}