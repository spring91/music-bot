package com.spotybot.botapi;

import com.spotybot.botapi.msghandl.InputMessageHandler;
import com.spotybot.model.enums.BotState;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Класс подборщик нужного обработчика сообщения по состоянию бота
 */
@Component
public class HandlerPicker {

    //Под каждое состояние бота кладется свой обработчик(БОЛЬШЕ ВАРИАТИВНОСТИ!)
    private final Map<Class<?>, InputMessageHandler> messageHandlers = new HashMap<>();

    /**
     * Заполнение поле мапы обработчиками
     *
     * @param messageHandlers - внедрение листа с классами обработчиками
     */
    @Autowired
    public HandlerPicker(List<InputMessageHandler> messageHandlers) throws NullPointerException {
        messageHandlers.forEach(handler -> this.messageHandlers.put(handler.getClass(), handler));
    }

    /**
     * Метод выбирает нужный хендлер и запускает у него метод обработки
     *
     * @param currentState параметр, с помощью которого выбирается обработчик
     */
    public BotApiMethod<?> processInputMessage(@NotNull BotState currentState, Message message) {
        return messageHandlers.get(currentState.getHandler()).handle(message);
    }
}