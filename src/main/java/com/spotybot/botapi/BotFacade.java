package com.spotybot.botapi;

import com.spotybot.dao.PersonDAO;
import com.spotybot.dao.SongStepDAO;
import com.spotybot.model.Person;
import com.spotybot.model.SongStepData;
import com.spotybot.model.enums.BotState;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import static com.spotybot.model.enums.BotState.*;

/**
 * Главный класс. Шлюз, который первым принимает update сообщение от telegram
 * и отправляет его обработчикам сообщений
 */
@Component
public class BotFacade {
    private final HandlerPicker handlerPicker;
    private final SongStepDAO dataCache;
    private final PersonDAO personDAO;

    @Autowired
    public BotFacade(HandlerPicker handlerPicker,
                     SongStepDAO dataCache,
                     PersonDAO personDAO) {
        this.handlerPicker = handlerPicker;
        this.dataCache = dataCache;
        this.personDAO = personDAO;
    }

    /**
     * Основная функция метода - генерация ответного сообщения.
     * Так же метод заполняет первоначальные данные о пользователе и его данных.
     */
    public BotApiMethod<?> handleUpdate(@NotNull Update update) {
        //Сообщение всегда должно приходить
        assert update.hasMessage();

        var message = update.getMessage();
        int userId = message.getFrom().getId().intValue();
        var person = personDAO.getPerson(userId);
        //если юзер не существует, то создаем и вносим пустые данные и выбираем состояние по сообщению
        BotState botState;
        if (person == null) {
            botState = getStateInputMsg(message);
            personDAO.savePerson(new Person(userId, botState));
            dataCache.saveData(userId, new SongStepData());
        } else {
            //восстановление состояния бота для пользователя
            botState = personDAO.getPerson(userId).getBotState();
        }
        //генерация сообщения в зависимости от состояния бота и сообщения пользователя
        return handlerPicker.processInputMessage(botState, message);
    }

    /**
     * Метод получает состояние входящего сообщения
     */
    private BotState getStateInputMsg(@NotNull Message message) {
        BotState botState;
        switch (message.getText()) {
            case "/start": {
                botState = SHOW_MAIN_MENU;
                break;
            }
            case "/download": {
                botState = ASK_ARTIST;
                break;
            }
            case "/getAlbumLink": {
                botState = ASK_ALBUM_ARTIST;
                break;
            }
            case "/help": {
                botState = SHOW_HELP_TEXT;
                break;
            }
            default: {
                botState = SHOW_MAIN_MENU;
            }
        }
        return botState;
    }
}