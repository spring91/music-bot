package com.spotybot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Ershov Daniil "daniilershov2001@gmail.com"
 * @version 1.0
 */
@SpringBootApplication
public class SpotyBotApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpotyBotApplication.class, args);
    }
}