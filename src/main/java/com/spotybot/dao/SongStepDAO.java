package com.spotybot.dao;

import com.spotybot.model.SongStepData;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Класс-хранилище, содержащий данные введенные юзером
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class SongStepDAO {
    private final Map<Integer, SongStepData> personHashMap = new HashMap<>();

    /**
     * Метод сохранения данных пользователя, может использоваться как перезапись данных.
     */
    public void saveData(Integer userId, SongStepData data) {
        personHashMap.put(userId, data);
    }

    /**
     * @param userId хранение данных пользователя происходит по id пользователя
     */
    public SongStepData getData(Integer userId) {
        return personHashMap.get(userId);
    }
}