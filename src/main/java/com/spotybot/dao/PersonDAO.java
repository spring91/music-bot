package com.spotybot.dao;

import com.spotybot.model.Person;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

/**
 * Класс-хранилище, содержащий ID всех пользователей, которые общаются с ботом
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class PersonDAO {
    private final List<Person> personStateHashMap = new LinkedList<>();

    /**
     * Метод записи юзера
     */
    public void savePerson(Person person) {
        personStateHashMap.add(person);
    }

    /**
     * Метод получения пользователя
     *
     * @param userId id телеграм юзера - уникален для каждого пользователя
     */
    public Person getPerson(int userId) {
        return personStateHashMap.get(userId);
    }
}